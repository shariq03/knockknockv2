﻿using System;

namespace KnockKnock.Core
{
    [Serializable]
    public enum TriangleType
    {
        Error = 0,
        Equilateral = 1,
        Isosceles = 2,
        Scalene = 3
    }
}

﻿using KnockKnock.Core.Contracts;
using System;

namespace KnockKnock.Core.Implementation
{
    public class MathFunctions : IMathFunctions
    {
        public long FibonacciNumber(long numInput)
        {
            long first = 0;
            long second = 1;
            long temp = 0;

            if (numInput == 0)
            {
                return 0;
            }
            else if (numInput > 92)
            {
                throw new OverflowException("Number Input can not exceed 92 it will create a negative response. ");
            }
            else if (numInput < 0)
            {
                if(numInput<-92)
                    throw new OverflowException("Number Input can not exceed -92");

                for (long i = numInput; i < 0; i++)
                {
                    temp = first;
                    first = second;
                    second = temp - second;
                }
            }
            else
            {
                //In numInput compute Fibonacci sequence iteratively.
                for (long i = 0; i < numInput; i++)
                {
                    temp = first;
                    first = second;
                    second = temp + second;
                }
            }
            return first;
        }
    }
}

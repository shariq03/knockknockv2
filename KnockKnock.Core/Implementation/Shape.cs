﻿using KnockKnock.Core.Contracts;

namespace KnockKnock.Core.Implementation
{
    public class Shape : IShape
    {
        /// <summary>
        /// Gets the shape.
        /// </summary>
        /// <param name="x">x</param>
        /// <param name="y">y</param>
        /// <param name="z">z</param>
        /// <returns></returns>
        public TriangleType GetShape(int x, int y, int z)
        {
            if (x < 1 || y < 1 || z < 1)
                return TriangleType.Error;
            else if (x + y <= z || y + z <= x || z + x <= y)
                return TriangleType.Error;
            else if (x == y && y == z)
                return TriangleType.Equilateral;
            else if (x == y || y == z || x == z)
                return TriangleType.Isosceles;
            else
                return TriangleType.Scalene;
        }
    }
}

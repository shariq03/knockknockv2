﻿using System;
using KnockKnock.Core.Contracts;
using System.Collections.Generic;
using System.Linq;

namespace KnockKnock.Core.Implementation
{
    public class StringManipulator : IStringManipulator
    {
        public string ReverseWords(string input)
        {
            if (!string.IsNullOrEmpty(input))
            {
                string reversedString = string.Empty;
                string reversedWords = string.Join(" ", from s in ReversedWordsBySpaces(input) select s);
                return reversedWords;
            }
            else
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Split phrase to word blocks over space char
        /// </summary>
        /// <param name="Word">Words phrase</param>
        /// <returns>Words List</returns>
        private IEnumerable<string> ReversedWordsBySpaces(string input)
        {
            string[] inputSpaceSplit = input.Split(' ');
            for (int i = 0; i < inputSpaceSplit.Length; i++)
            {
                yield return ReverseSingleWord(inputSpaceSplit[i]);
            }
        }

        /// <summary>
        /// Reverses the string.
        /// </summary>
        /// <param name="Word">The input.</param>
        /// <returns></returns>
        private string ReverseSingleWord(string Word)
        {
            char[] cArray = Word.ToCharArray();
            string reversedString = string.Empty;
            for (int i = cArray.Length - 1; i > -1; i--)
            {
                reversedString += cArray[i];
            }
            return reversedString;
        }

       
    }
}

﻿using KnockKnock.Core.Contracts;
using System;

namespace KnockKnock.Core.Implementation
{
    public class TokenRepository : ITokenRepository
    {
        /// <summary>
        /// Get the token.
        /// Currently hard coded but able to get the token from db/config
        /// </summary>
        /// <returns></returns>
        public Guid GetToken()
        {
            return Guid.Parse("8527eaf5-5cee-41fc-a9cd-03b79d30e56b");
        }
    }
}

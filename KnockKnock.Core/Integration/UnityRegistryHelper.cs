﻿using KnockKnock.Core.Contracts;
using KnockKnock.Core.Implementation;
using Microsoft.Practices.Unity;

namespace KnockKnock.Core.Integration
{

    public class UnityRegistryHelper : IUnityRegistryHelper
    {
        private static UnityContainer container = new UnityContainer();
        /// <summary>
        /// Registers the unity container.
        /// </summary>
        /// <returns></returns>
        public UnityContainer RegisterContainer()
        {
            if (!container.IsRegistered(typeof(ITokenRepository)))
            {
                container.RegisterType<ITokenRepository, TokenRepository>();
            }
            if (!container.IsRegistered(typeof(IMathFunctions)))
            {
                container.RegisterType<IMathFunctions, MathFunctions>();
            }
            if (!container.IsRegistered(typeof(IStringManipulator)))
            {
                container.RegisterType<IStringManipulator, StringManipulator>();
            }
            if (!container.IsRegistered(typeof(IShape)))
            {
                container.RegisterType<IShape, Shape>();
            }
            return container;
        }
    }
}
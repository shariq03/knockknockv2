﻿using Microsoft.Practices.Unity;

namespace KnockKnock.Core.Integration
{
    public interface IUnityRegistryHelper
    {
        /// <summary>
        /// Registers the Unity container.
        /// </summary>
        /// <returns></returns>
        UnityContainer RegisterContainer();
    }
}

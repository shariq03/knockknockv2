﻿namespace KnockKnock.Core.Integration
{
    public class IntegrationManager
    {
        private static IUnityRegistryHelper _register;
        //make construction private so it can not be intantiated
        private IntegrationManager()
        {

        }
        /// <summary>
        /// Gets the singleton instance.
        /// </summary>
        /// <value>
        /// The instance.
        /// </value>
        public static IUnityRegistryHelper Instance
        {
            get
            {
                if (_register == null)
                {
                    _register = new UnityRegistryHelper();
                }
                return _register;
            }
        }
    }
}

﻿namespace KnockKnock.Core.Contracts
{
    public interface IMathFunctions
    {
        /// <summary>
        /// Fibonaccis the series.
        /// </summary>
        /// <returns></returns>
        long FibonacciNumber(long numInput);
    }
}

﻿namespace KnockKnock.Core.Contracts
{
    public interface IStringManipulator
    {
        /// <summary>
        /// Reverses the Words.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns></returns>
        string ReverseWords(string input);
    }
}

﻿using System;

namespace KnockKnock.Core.Contracts
{
    public interface ITokenRepository
    {
        /// <summary>
        /// Get the token.
        /// </summary>
        /// <returns></returns>
        Guid GetToken();
    }
}

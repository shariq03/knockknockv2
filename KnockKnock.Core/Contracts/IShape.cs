﻿namespace KnockKnock.Core.Contracts
{
    public interface IShape
    {
        /// <summary>
        /// Gets the shape.
        /// </summary>
        /// <param name="x">x</param>
        /// <param name="y">y</param>
        /// <param name="z">z</param>
        /// <returns></returns>
        TriangleType GetShape(int x,int y,int z); 
    }
}

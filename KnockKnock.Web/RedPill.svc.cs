﻿using System;
using System.ServiceModel;
using Microsoft.Practices.Unity;
using KnockKnock.Core.Integration;
using KnockKnock.Core.Contracts;
using KnockKnock.Core;

namespace KnockKnock.Web
{
    [ServiceBehavior(IncludeExceptionDetailInFaults = true)]
    public class RedPill : IRedPill
    {
        ITokenRepository _token;
        IStringManipulator _manipulator;
        IMathFunctions _mathFunc;
        IShape _shape;
        /// <summary>
        /// Initializes a new instance of the <see cref="RedPill"/> class.
        /// </summary>
        public RedPill()
        {
            _token = IntegrationManager.Instance.RegisterContainer().Resolve<ITokenRepository>();
            _manipulator = IntegrationManager.Instance.RegisterContainer().Resolve<IStringManipulator>();
            _mathFunc = IntegrationManager.Instance.RegisterContainer().Resolve<IMathFunctions>();
            _shape = IntegrationManager.Instance.RegisterContainer().Resolve<IShape>();
        }
        public Guid WhatIsYourToken()
        {
            return _token.GetToken();
        }
        public string ReverseWords(string s)
        {
            return _manipulator.ReverseWords(s);
        }

        public long FibonacciNumber(long n)
        {
            return _mathFunc.FibonacciNumber(n);
        }

        public TriangleType WhatShapeIsThis(int x, int y, int z)
        {
            return _shape.GetShape(x, y, z);
        }
    }

}
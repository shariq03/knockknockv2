﻿using KnockKnock.Core;
using System;
using System.ServiceModel;

namespace KnockKnock.Web
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IRedPill" in both code and config file together.

    [ServiceContract(Namespace = "http://KnockKnock.readify.net")]
    public interface IRedPill
    {
        [OperationContract]
        Guid WhatIsYourToken();

        [OperationContract]
        string ReverseWords(string s);

        [OperationContract]
        long FibonacciNumber(long n);

        [OperationContract]
        TriangleType WhatShapeIsThis(int x, int y, int z);
    }
}

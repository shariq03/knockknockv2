﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using KnockKnock.Core.Contracts;
using KnockKnock.Core.Integration;
using Microsoft.Practices.Unity;

namespace KnockKnock.Tests
{
    [TestClass]
    public class StringManipulatorTests
    {
        private IStringManipulator _manipulator = IntegrationManager.Instance.RegisterContainer().Resolve<IStringManipulator>();

        [TestMethod]

        public void StringManipulator_ReverseInputStringShouldNotNull()
        {
            var result = _manipulator.ReverseWords(null);
            Assert.AreEqual("", result);
        }

        [TestMethod]
        public void StringManipulator_ReverseInputStringShouldNotBeEmpty()
        {
            var result = _manipulator.ReverseWords("");
            Assert.AreEqual("", result);
        }

        [TestMethod]
        public void StringManipulator_CompareInputWordsWithReversedWords()
        {
            var result = _manipulator.ReverseWords("We use the yield keyword in several contexts");
            Assert.AreEqual("eW esu eht dleiy drowyek ni lareves stxetnoc", result);
        }
    }
}

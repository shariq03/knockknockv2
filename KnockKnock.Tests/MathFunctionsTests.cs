﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using KnockKnock.Core.Contracts;
using KnockKnock.Core.Integration;
using Microsoft.Practices.Unity;

namespace KnockKnock.Tests
{
    [TestClass]
    public class MathFunctionsTests
    {
        private IMathFunctions _mathFunc = IntegrationManager.Instance.RegisterContainer().Resolve<IMathFunctions>();

        [TestMethod]
        public void MathFunctions_FibonacciNumberInputShouldNotBeZero()
        {
            var number = _mathFunc.FibonacciNumber(0);
            Assert.AreEqual(0, number);
        }

        [TestMethod]
        public void MathFunctions_FibonacciCompareComputedNumber()
        {
            var result = _mathFunc.FibonacciNumber(8);
            Assert.AreEqual(21, result);
        }

        [TestMethod]
        public void MathFunctions_FibonacciComputedNumberUpto92()
        {
            var result = _mathFunc.FibonacciNumber(92);
            Assert.AreEqual(7540113804746346429, result);
        }
        [TestMethod]
        public void MathFunctions_FibonacciComputedNumberUptoNegative92()
        {
            var result = _mathFunc.FibonacciNumber(-92);
            Assert.AreEqual(-7540113804746346429, result);
        }

        [TestMethod]
        public void MathFunctions_FibonacciCompareComputedNegativeNumber()
        {
            var result = _mathFunc.FibonacciNumber(-8);
            Assert.AreEqual(-21, result);
        }

        [TestMethod]
        [ExpectedException (typeof(OverflowException))]
        public void MathFunctions_FibonacciExpectOverflowException()
        {
            var result = _mathFunc.FibonacciNumber(93);
        }

        [TestMethod]
        [ExpectedException(typeof(OverflowException))]
        public void MathFunctions_FibonacciExpectNegativeOverflowException()
        {
            var result = _mathFunc.FibonacciNumber(-93);
        }
    }
}

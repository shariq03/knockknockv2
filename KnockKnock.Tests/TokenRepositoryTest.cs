﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using KnockKnock.Core.Integration;
using Microsoft.Practices.Unity;
using KnockKnock.Core.Contracts;

namespace KnockKnock.Tests
{
    [TestClass]
    public class TokenRepositoryTest
    {
        private ITokenRepository _token = IntegrationManager.Instance.RegisterContainer().Resolve<ITokenRepository>();
        [TestMethod]
        public void TokenRepository_TokenShouldNotbeNullOrEmpty()
        {
            Guid token = _token.GetToken();
            Assert.AreNotEqual(Guid.Empty, token);
        }

        [TestMethod]
        public void TokenRepository_TokenShouldBeEqualToVerifiedRedifyToken()
        {
            Guid token = _token.GetToken();
            Assert.AreEqual("8527eaf5-5cee-41fc-a9cd-03b79d30e56b", token.ToString());
        }       
    }
}
